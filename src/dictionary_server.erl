%%%-------------------------------------------------------------------
%%% @author kierenmillar
%%%-------------------------------------------------------------------
-module(dictionary_server).
-author("kierenmillar").

%% API
-export([start/0, stop/0, size/0, clear/0, insert/2, lookup/1, remove/1]).

start() -> register(dictionary, spawn(fun () -> run([]) end)).

stop() ->
  case whereis(dictionary) of
    undefined -> no_effect;

    _ -> call({self(),stop})
  end.

insert(Key, Value) ->
  call({self(), insert, Key, Value}).

remove(Key) ->
  call({self(), remove, Key}).

lookup(Key) ->
  call({self(), lookup, Key}).

clear() ->
  call({self(), clear}).

size() ->
  call({self(), size}).



call(Message) ->
  dictionary ! Message,
  receive
    {Result} -> Result
  end.

run(DictionaryList) ->
  receive
    {RespondPid, stop} -> respond(RespondPid, ok);

    {RespondPid, insert, Key, Value} ->
      respond(RespondPid, ok),
      run(lists:keystore(Key, 1, DictionaryList, {Key, Value}));

    {RespondPid, remove, Key} ->
      respond(RespondPid, ismember(Key, DictionaryList)),
      run(lists:keydelete(Key, 1, DictionaryList));

    {RespondPid, lookup, Key} ->
      respond(RespondPid, lookuplist(DictionaryList, Key)),
      run(DictionaryList);

    {RespondPid, clear} ->
      respond(RespondPid, dictionary_cleared),
      run([]);

    {RespondPid, size} ->
      respond(RespondPid,length(DictionaryList)),
      run(DictionaryList)


  end.

respond(RespondPid, Message) ->
  RespondPid ! {Message}.


lookuplist(DictionaryList, Key) ->
  case lists:keyfind(Key, 1, DictionaryList) of
    {Key, Value} -> {ok, Value};
    _ -> notfound
  end.

ismember(Key, DictionaryList) ->
  case lists:keymember(Key, 1, DictionaryList) of
    true -> ok;

    false -> notfound
  end.



