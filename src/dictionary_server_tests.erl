%%%-------------------------------------------------------------------
%%% @author kierenmillar
%%%-------------------------------------------------------------------
-module(dictionary_server_tests).
-author("kierenmillar").

-include_lib("eunit/include/eunit.hrl").

start_test() ->
  dictionary_server:start(),
  ?assertError(badarg, dictionary_server:start()).

stop_test() ->
  ?assertEqual(ok,dictionary_server:stop()),
  ?assertEqual(no_effect, dictionary_server:stop()).

insert_test() ->
  dictionary_server:start(),
  ?assertEqual(ok,dictionary_server:insert(testkey, testvalue)),
  ?assertEqual({ok, testvalue}, dictionary_server:lookup(testkey)),
  dictionary_server:insert(testkey, replacedvalue),
  ?assertNotEqual({ok, testvalue}, dictionary_server:lookup(testkey)),
  ?assertEqual({ok, replacedvalue}, dictionary_server:lookup(testkey)).

remove_test() ->
  dictionary_server:insert(testkey2, testvalue2),
  ?assertEqual(ok, dictionary_server:remove(testkey2)),
  ?assertEqual(notfound, dictionary_server:remove(testkey2)).

lookup_test() ->
  ?assertEqual({ok, replacedvalue}, dictionary_server:lookup(testkey)),
  ?assertEqual(notfound, dictionary_server:lookup(testkey2)).

clear_and_size_test() ->
  ?assertEqual(1, dictionary_server:size()),
  dictionary_server:clear(),
  ?assertEqual(0, dictionary_server:size()),
  dictionary_server:stop().





